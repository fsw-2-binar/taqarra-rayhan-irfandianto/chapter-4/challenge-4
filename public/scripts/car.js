class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="col ">
    <div class="card">
        <img src="${this.image}" class="card-img-top mobil-gan" alt="..." width>
        
        <div class="card-body">
            <p class="card-text">Nama/Tipe Mobil</p>
            <strong>
                <p>Rp. ${this.rentPerDay}/ hari</p>
            </strong>
            <p> ${this.description} </p>
            <ul class="m-0 p-0">
                <li><img src="./image/fi_users.png" alt="">${this.capacity}</li>
                <li style="width: 140px;"><img src="./image/fi_settings.png" alt=""">${this.transmission}</li>
                <li><img src="./image/fi_calendar.png" alt="">${this.year}</li>
            </ul>
            <div class="d-grid gap-2 mt-3 kotak-tombol">
            <button class="btn btn-success ukuran-tombol" type="button">Pilih Mobil</button>
          </div>
        </div>
    </div>
</div>

    `;
  }
}
