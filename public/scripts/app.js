class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.buttonsubmit=document.getElementById("submitData");
    this.tipedriver=document.getElementById("tipeDriver");
    this.sewa=document.getElementById("tanggalSewa")
    this.waktu=document.getElementById("waktuJemput")
    this.jumlah=document.getElementById("jumlah")
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.buttonsubmit.onclick = this.run;
    this.run
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add('col-12', 'col-lg-4', 'col-md-6');
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };


  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
    
    this.buttonsubmit.addEventListener('click',async(e)=>{
      let tipedriver=this.tipedriver.value;
      let sewa=this.sewa.value
      let waktu = this.waktu.value
      let jumlah = this.jumlah.value
      this.clear()
      // console.log(tipedriver,sewa,waktu,jumlah)
      let cars=await Binar.listCars((car) => {
        let result = true;

        let dateTime = sewa + "T" + waktu+":00"
        console.log(dateTime)
        if((!isNaN(Date.parse(dateTime)))&& (!isNaN(parseInt(jumlah)))){
          result=(car.availableAt.getTime()>= Date.parse(dateTime)) && (car.capacity === parseInt(jumlah))

        }

        if(!isNaN(Date.parse(dateTime))){
          result = (car.availableAt.getTime()>= Date.parse(dateTime))
        }

        if(!isNaN(parseInt(jumlah))){
          result = (car.capacity === parseInt(jumlah))
        }
        return result

      })
      console.log(cars)
      Car.init(cars)
    })
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };

}
